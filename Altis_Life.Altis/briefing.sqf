waitUntil {!isNull player && player == player};
if(player diarySubjectExists "rules")exitwith{};

player createDiarySubject ["changelog","Change Log"];
player createDiarySubject ["serverrules","General Rules"];
player createDiarySubject ["policerules","Police Procedures/Rules"];
player createDiarySubject ["safezones","Safe Zones (No Killing)"];
//player createDiarySubject ["civrules","Civilian Rules"];
player createDiarySubject ["illegalitems","Illegal Activity"];
//player createDiarySubject ["gangrules","Gang Rules"];
//player createDiarySubject ["terrorrules","Terrorism Rules"];
player createDiarySubject ["controls","Controls"];

	player createDiaryRecord["changelog",
		[
			"Official Change Log",
				"
					The official change log can be found on the BIS forums (search Altis Life RPG)
				"
		]
	];
	
	player createDiaryRecord["changelog",
		[
			"Custom Change Log",
				"
					This section is meant for people doing their own edits to the mission, DO NOT REMOVE THE ABOVE.
				"
		]
	];

		player createDiaryRecord ["serverrules",
		[
						"Regeln", 
							"Allgemeine Regeln<br/>
							<br/>
							§1 Exploits, Hacks, Bugsusing<br/>
							<br/>
							1. Das Nutzen von Bugs, Hacks, Cheats, Exploits etc. ist verboten und wird mit einem Ban/Kick bestraft.<br/>
							2. Wenn ein Bug bekannt wird ist dieser im Forum oder bei einem Admin zu melden.<br/>
							<br/>
							§2 Safezones<br/>
							<br/>
							1. Kavala Marktplatz<br/>
							2. Rebellen HQ<br/>
							3. VIP Shop<br/>
							4. Jeder ATM (Umkreis 100 Meter)<br/>
							5. Jede Garage (Umkreis 100 Meter)<br/>
							6. Police HQs sind trotz ATM keine Safezone, alles andere schon.<br/>
							7. Befindet man sich in einer Verfolgungsjagd, gelten Safezones nicht als Safezone! (Gilt für Cops wie für Zivilisten)<br/>
							8. Campen im Umkreis von Safezones ist verboten.<br/>
							9. In einer Safezone sind jegliche kriminellen Handlungen verboten. <br/>
							<br/>
							§3 One-Life-Regeln<br/>
							<br/>
							1. Jeder darf genau einmal an einem Event teilnehmen. Nach seinem Tod darf der Teilnehmer nicht an den Ort des Geschehens zurückkehren bis das Event beendet ist.<br/>
							2. Ein Event beginnt mit Eröffnung des RP und endet mit dem Ausgang dessen.<br/>
							3. Suizid ist kein Tod.<br/>
							<br/>
							§4 Absichtliche Zerstörung von Fahrzeugen<br/>
							<br/>
							1. Das vorsätzliche Zerstören von Fahrzeugen ohne RPhintergrund ist verboten.<br/>
							2. Das absichtliche Rammen von Fahrzeugen außerhalb eines Events um sie zu zerstören ist verboten.<br/>
							<br/>
							§5 Kommunikation<br/>
							<br/>
							1. Das Sprechen im Side-Chat ist verboten.<br/>
							2. Spammen ist verboten.<br/>
							3. Das Posten von Links (Fremdwerbung etc.) ist verboten.<br/>
							4. Beleidigungen, Rassismus, Antisemitismus und pornografische Sprache ist verboten und wird mit einem Ban/Kick bestraft.<br/>
							<br/>
							§6 Random Deathmatch (RDM)<br/>
							<br/>
							1. Das wahllose Töten von Spielern ohne RP-Hintergrund ist verboten.<br/>
							2. Solltest du als Zivilist in ein Event geraten und sterben gilt dies nicht als RDM.<br/>
							3. Das Verteidigen von Gangmitgliedern ist erlaubt, sofern diese durch ein Clantag gekennzeichnet sind.<br/>
							4. Sollte sich ein Polizist im Rebellengebiet befinden, so ist dieser als Vogelfrei zu betrachten (Er darf ohne RP Grund getötet werden).<br/>
							<br/>
							<br/>
							Gesetze für Zivilisten<br/>
							<br/>
							§1 Illegale Gegenstände<br/>
							<br/>
							1. Liste illegaler Gegenstände:<br/>
							Fahrzeuge:<br/>
							Hunter (alle Ausführungen)<br/>
							Ifrit (alle Ausführungen)<br/>
							Strider (alle Ausführungen)<br/>
							Marid (alle Ausführungen)<br/>
							Rebellenoffroad (beide Ausführungen)<br/>
							Jegliche Polizeifahrzeuge<br/>
							Waffen:<br/>
							Polizeiwaffen<br/>
							Rebellenwaffen<br/>
							Gegenstände<br/>
							Alle formen von Drogen:<br/>
							- Frösche und somit auch LSD<br/>
							- Mohn und somit auch Heroin<br/>
							- Kokablätter und somit auch Kokain<br/>
							- Cannabis und somit auch Marihuana<br/>
							Blue Look<br/>
							Goldbarren aus dem Tresor der Staatsbank<br/>
							[-] Illegales Verarbeiten<br/>
							+ Es ist illegal Pfirsiche zu Schnaps zu verändern. Jedoch ist es erlaubt Pfirsiche bzw. fertigen Pfirsichschnaps bei sich zu führen!<br/>
							<br/>
							§2 Einmischung in Polizeiaktionen<br/>
							<br/>
							1. Das dauerhafte spionieren und / oder stalken von Polizisten ist verboten.<br/>
							2. Das dauerhafte Blocken von Polizisten um diese daran zu hindern ihre Pflicht zu tun ist verboten.<br/>
							<br/>
							§3 Verhalten bei polizeilichen Maßnahmen<br/>
							<br/>
							1. Den Anweisungen der Polizei ist Folge zu leisten.<br/>
							<br/>
							§4 Verkehrsregeln<br/>
							<br/>
							1. Es gelten die Regeln der StVo<br/>
							2. Jeder Fahrzeugführer hat Werkzeugkästen und Erste-Hilfe-Kästen mitzuführen.<br/>
							3. Das Überfliegen von Städten unterhalb von 400m ist verboten. Bei Zuwiederhandlung darf das Feuer eröffnet werden.<br/>
							4. Das Landen von Helikoptern in Städten und auf Straßen ist verboten. Ausnahmeregelungen können durch den höchsten diensthabenden Polizisten erteilt werden.<br/>
							<br/>
							§5 Umgang mit Waffen<br/>
							<br/>
							1. Solltet ihr eine illegale Waffe tragen, müsst ihr damit rechnen, dass ein Polizist sie entfernen muss.<br/>
							2. In Städten ist die Waffe generell geschultert zu tragen.<br/>
							3. Sollte eine Waffe in eurem Besitz sein, wenn die Polizei euch wegen einer Straftat festnimmt, darf diese beschlagnahmt werden.<br/>
							4. Wer ohne Waffenlizenz angetroffen wird, dem darf die Waffe durch die Polizei abgenommen werden.<br/>
							<br/>
							§6 Vehicle Deathmatch (VDM)<br/>
							<br/>
							1. Das Überfahren von Spielern ist verboten (Dies gilt auch obwohl man dadurch nicht mehr sterben kann).<br/>
							2. Solltet du in eine Schießerei geraten, heißt dies nicht, dass du andere Spieler überfahren darfst.<br/>
							3. Das Überfahren, egal wann oder wo, ist verboten und wird mit einem Kick/Ban bestraft!<br/>
							4. Absichtliches Rammen zählt nicht als VDM. Sollte jedoch das gerammte Fahrzeug explodieren, muss das Fahrzeug sowie der gesamte Inhalt und die Ausrüstung der Insassen erstattet werden. nsen erstatten.<br/>
							<br/>
							§7 Luftverkehr<br/>
							<br/>
							1. Das absichtliche Rammen von Helikoptern. (In andere Helikopter, Vehikel, Gebäude usw.) wird mit einem Ban bestraft.<br/>
							2. Das Überfliegen von Städten unterhalb der Mindestflughöhe von 400m. wird mit einem Ticket bestraft.<br/>
							3. In Städten gilt ein absolutes Landeverbot, außer mit einer erteilten Landeerlaubnis.<br/>
							<br/>
							§8 Sperrzonen<br/>
							<br/>
							1. Jegliches Polizeihauptquartier ist eine Sperrzone. Somit ist das eindringen verboten und man darf dort getötet oder festgenommen werden. Ein Polizist kann jedoch das Eindringen gewähren.<br/>
							2. Das Rebellengebiet ist eine Sperrzone für jeden Cop. <br/>
							<br/>
							§9 Gang Gebiete<br/>
							<br/>
							1. Jeder der sich einem Ganggebiet nähert, welches nicht von seiner eigenen Gang ist, darf ohne Ankündigung erschossen werden. Sollte er jedoch das Gebiet angreifen wollen, so muss er dies 5 Minuten vorher mit RP Ankündigen.<br/>
							<br/>
							§10 Zusatz<br/>
							<br/>
							1. Die Gesetze von Altis sind verpflichtend.<br/>
							2. Bei Verstoß gegen die Gesetze wird nach Bußgeldkatalog bestraft. <br/>
							<br/>
							<br/>
							Gesetze für Rebellen<br/>
							<br/>
							1. Für Rebellen Gruppierungen ist es zwingend notwendig, dass jedes Mitglied, den Namen der Gruppierung in VOR seinem Namen Trägt<br/>
							2. Sollte ein Kampf zwischen zwei Gruppierungen stattfinden, so muss Unterstützung von anderen Gruppierungen Angekündigt werden.<br/>
							3. Rebellen dürfen jede Stadt/Ort/Gebiet bis auf Kavala oder Sofia einnehmen. Dies sollte jedoch einem Admin davor gemeldet werden.<br/>
							4. Wenn ein Zivilist, welcher keiner Gruppierung angehört mit in das geschehen eingreifen will, so muss er dies mit einer Nachricht ankündigen oder das Clantag der zu unterstützenden Gruppierung tragen.<br/>
							5. Es ist Rebellen untersagt Personen zu beklauen, welche Fahrzeuge des Autohändlers fahren/benutzen. <br/>
							<br/>
							§2 Rebellenverhalten<br/>
							<br/>
							1. Rebellen sind keine Banditen und sollten sich dementsprechend auch nicht wie solche benehmen.<br/>
							2. Rebellenangriffe müssen als Gang ausgeführt werden.<br/>
							3. Rebellen richten sich gegen den Staat und nicht gegen Zivilisten.<br/>
							<br/>
							<br/>
							§2 Marid und Titan<br/>
							<br/>
							1. Mit dem Marid darf nicht geschossen werden, es sei denn, die Gegnerseite hat ein dergleiches Fahrzeug.<br/>
							2. Sollte Punkt 1 eintreten, so darf nur auf das Fahrzeug geschossen werden.<br/>
							3. Mit der Titan darf nur auf gepanzerte Fahrzeuge geschossen werden.<br/>
							[-] Gepanzerte Vehicles:<br/>
							- Mohawk, Ghost Hawk, Mi-290 Taru, CH-67, Strider (alle Ausführungen), Hunter (alle Ausführungen), Ifrit (alle Ausführungen), Marid.<br/>
							<br/>
							<br/>
							<br/>
							Gesetze für Polizisten<br/>
							<br/>
							§1 Allgemeines Verhalten<br/>
							<br/>
							1. Polizisten haben sich dauerhaft im TS aufzuhalten.<br/>
							2. Jeder Polizist hat sich an die geltenden Gesetze in Altis zu halten.<br/>
							3. Die Polizei ist Freund und Helfer und sollte sich auch entsprechend verhalten.<br/>
							<br/>
							§2 Patroullien<br/>
							<br/>
							1. Jeder Polizist sollte im ihm zugeteilten Gebiet patroullieren.<br/>
							2. Bei Patrouillen dürfen Bürger ohne jeglichen Grund kontrolliert und durchsucht werden.<br/>
							3. Auf Patroulliengängen ist die Waffe stets gesenkt zu halten.<br/>
							<br/>
							§3 Fahrzeugkontrollen und Checkpoints<br/>
							<br/>
							1. Die Polizei darf sowohl feste als auch mobile Straßensperren errichten um Fahrzeugkontrollen durchzuführen.<br/>
							2. Bei einer Fahrzeugkontrolle ist für die Absicherung der Polizeikräfte zu sorgen.<br/>
							3. Ein Checkpoint muss aus mindestens drei Polizisten und zwei Fahrzeugen bestehen.<br/>
							4. Solange ein Checkpoint besetzt ist dürfen Polizisten sich an diesem wiederbeleben lassen.<br/>
							<br/>
							§4 Einsatz von Waffen<br/>
							<br/>
							1. Jeder Polizist ist angehalten auf nicht-tödliche Waffen zurückzugreifen.<br/>
							2. Die Polizei sollte versuchen, jeden Verdächtigen festzunehmen, nicht zu töten.<br/>
							3. Der Einsatz von tödlicher Munition ist Rekruten nur mit Anweisung eines ranghöheren Polizisten erlaubt.<br/>
							4. Waffen sind generell gesenkt zu tragen.<br/>
							5. Polizisten dürfen sich mit tödlicher Munition zur Wehr setzen falls sie angegriffen werden.<br/>
							<br/>
							§5 Banküberfälle<br/>
							<br/>
							1. Sind mindestens fünf Polizisten im Dienst muss eingeschritten werden.<br/>
							2. Patroullierende Polizisten begeben sich ebenfalls umgehend zum Bankraub.<br/>
							3. Jede Möglichkeit die Bankräuber zu verhaften sollte genutzt werden.<br/>
							<br/>
							§6 Razzien, Raids und Camping<br/>
							<br/>
							1. Bei einer Razzia / einem Raid müssen mindestens vier Beamte anwesend sein.<br/>
							2. Bei einer Razzia / einem Raid darf jede Person zunächst kontrolliert und festgehalten werden.<br/>
							3. Nach einer Razzia / einem Raid darf das entsprechende Gebiet für mindestens 35 Minuten nicht erneut betreten werden.<br/>
							4. Das becampen von illegalen Gebieten ist untersagt.<br/>
							<br/>
							§7 Beschlagnahmung<br/>
							<br/>
							1. Fahrzeuge, welche nicht Ordnungsgemäß abgestellt wurden, dürfen nach mehr als 5 Minuten stillstand abgeschleppt werden.<br/>
							2. Fahrzeuge welche Ordnungsgemäß auf Parkplätzen abgestellt und unbeschädigt sind dürfen nicht abgeschleppt werden.<br/>
							3. Fahrzeuge welche für schwere Straftaten eingesetzt werden, dürfen von der Polizei zerstört werden.<br/>
							4. Illegale Fahrzeuge dürfen sofort zerstört werden.<br/>
							<br/>
							§8 Festnahmen und Bußgelder<br/>
							<br/>
							1. Jeder Polizist ist angehalten, Bußgelder statt Gefängnisstrafen zu verhängen.<br/>
							2. Der Bußgeldkatalog ist hierbei bindend.<br/>
							3. Wiederholungstäter sind zu inhaftieren.<br/>
							4. Jeder Bürger hat das Recht zu erfahren, wieso er festgenommen wird.<br/>
							4. Jeder Bürger hat das Recht darauf, seine Rechte vorgelesen zu bekommen.<br/>
							5. Sollte ein Bußgeld verhängt und bezahlt werden ist der Gefangene freizulassen, außer es handelt sich um ein Vergehen, welches ein Bußgeld und eine Haftstrafe zur Folge hat.<br/>
							<br/>
							<br/>
							§9 One-Life-Regel<br/>
							<br/>
							1. Die One-Life-Regel für Cops gilt nur dann, wenn mehr als 5 Polizisten online sind.<br/>
							2. Sind 5 oder weniger Cops online, hat ein Polizist zwei Leben.<br/>
							3. Das Wiederbeleben von Cops während eines Events ist verboten!<br/>
							<br/>
							§11 Zusätze<br/>
							<br/>
							1. Die Polizeigesetze sind bindend.<br/>
							2. Bei wiederholtem Verstoß gegen die Polizeigesetze wird der Polizist unehrenhaft aus dem Dienst entlassen.<br/>
							<br/>
							Gesetze bzgl. Events<br/>
							<br/>
							<br/>
							§1 Eventeröffnung<br/>
							<br/>
							1. “Halt Überfall” gilt nicht als Eventeröffnung!<br/>
							2. Eventeröffnungen dürfen nicht im Side-Chat gemacht werden. Ausgenommen hiervon sind Stadteinnahmen!<br/>
							<br/>
							§2 Geiselnahmen<br/>
							<br/>
							1. Bei Geiselnahmen muss mit dem Opfer kommuniziert werden.<br/>
							2. Für das Wohl der Geiseln ist zu sorgen.<br/>
							3. Sobald die Geisel genommen wurde müssen Forderungen gestellt werden.<br/>
							4. Sowohl von Cops als auch von den Geiselnehmern sollten Verhandlungen geführt werden. Hierfür sollten beide Parteien aufeinander zugehen!<br/>
							<br/>
							<br/>
							Zusatz<br/>
							<br/>
							§1 Helikopter<br/>
							<br/>
							1. Es darf nicht aus Helikoptern heraus geschossen werden.<br/>
							2. Es dürfen keine Fahrzeuge, welche nicht seinem Eigentum angehören, geliftet werden. AUSNAHME es liegt eine zustimmung vor.<br/>
							3. Es dürfen keine Fahrzeuge per Lift abgeworfen werden.<br/>
							<br/>
							§2 Fahrzeuge<br/>
							<br/>
							1. Wenn man aus einem Fahrzeug heraus schießt, dann nur per ankündigung!<br/>
							2. Es ist gestattet auf die Reifen des Opers zu schießen. Sollte dieses dabei umkommen ist es RDM!<br/>
							3. Es ist nicht gesattet per Helikopter-Lift einen schwebenden-Offroad-HMG zu erzeugen und aus diesem zu schießen!<br/>
							<br/>
							<br/>
							Die aktuellsten Regeln stehen immer im Forum. Die ingame Regeln sind nicht immer aktuell!<br/>
							Wir behalten uns vor diese Regeln jederzeit zu ändern!<br/>
							<br/>
							"
		]
	];
	
	player createDiaryRecord["safezones",
		[
			"Bußgeldkatalog",
				"
					Für die aktuellste Version schau bitte auf der Webseite vorbei.<br/>
					Invonion.eu
				"
		]
	];
	
// Controls Section

	player createDiaryRecord ["controls",
		[
			"Steuerung",
				"
				Y: Öffnet das Player Menü<br/>
				U: Aufschließen und Abschließen<br/>
				T: Fahrzeug Inventar<br/>
				Left Shift + R: Fesseln <br/>
				Left Shift + G: Bewusstlos Schlage<br/>
				Left Shift + B: Sich ergeben<br/>
				Q: Mit der Pickaxe Farmen<br/>
				Left Windows: Main Interaction key which is used for picking up items/money, interacting with cars (repair,etc) and for cops to interact with civilians. Can be rebound to a single key like H by pressing ESC->Configure->Controls->Custom->Use Action 10<br/>
				"
		]
	];