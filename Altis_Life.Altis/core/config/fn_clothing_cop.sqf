#include <macro.h>
/*
	File: fn_clothing_cop.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master config file for Cop clothing store.
*/
private["_filter","_ret"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Altis Police Department Shop"];

_ret = [];
switch (_filter) do
{
	//Uniforms
	case 0:
	{
		if(__GETC__(life_coplevel) > 0) then
		{
		    _ret set[count _ret,["U_Rangemaster","Anwärter Uniform",1]];
		};
		if(__GETC__(life_coplevel) > 1) then
		{
			_ret set[count _ret,["U_B_CombatUniform_mcam","Polizei Uniform",1]];
		};
		if(__GETC__(life_sek) == 1)then{
			_ret set[count _ret,["U_B_CombatUniform_mcam","SEK",1]];
		};
		
		
	};
	
	//Hats
	case 1:
	{
	    if(__GETC__(life_coplevel) > 3) then
		{
		    _ret set[count _ret,["H_CrewHelmetHeli_B",nil,1000]];
		};
		
        if(__GETC__(life_coplevel) > 5) then
		{
			_ret set[count _ret,["H_Watchcap_blk",nil,1000]];
		};
		
		if(__GETC__(life_coplevel) > 6) then
		{
			_ret set[count _ret,["H_Beret_blk_POLICE",nil,1000]];
		};
		
		if(__GETC__(life_sek) == 1) then{
			_ret set[count _ret,["H_HelmetSpecB_blk",nil,1000]];
			_ret set[count _ret,["H_CrewHelmetHeli_B",nil,1000]];
		};
		
		
	};
	
	//Glasses
	case 2:
	{
		_ret = 
		[
			["G_Shades_Black",nil,100],
			["G_Shades_Blue",nil,100],
			["G_Sport_Blackred",nil,100],
			["G_Sport_Checkered",nil,100],
			["G_Sport_Blackyellow",nil,100],
			["G_Sport_BlackWhite",nil,100],
			["G_Aviator",nil,100],
			["G_Squares",nil,100],
			["G_Lowprofile",nil,100],
			["G_Diving",nil,100],
			["G_Combat",nil,100]
		];
	};
	
	//Vest
	case 3:
	{
	    if(__GETC__(life_coplevel) > 0) then
		{
		    _ret set[count _ret,["V_Rangemaster_belt",nil,1000]];
		};
		
		if(__GETC__(life_coplevel) > 1) then
		{
			_ret set[count _ret,["V_Chestrig_blk",nil,1000]];
		};

		if(__GETC__(life_coplevel) > 3) then
		{
			_ret set[count _ret,["V_TacVest_blk",nil,1000]];
			_ret set[count _ret,["V_TacVest_blk_POLICE",nil,1000]];
		};

		if(__GETC__(life_coplevel) > 5) then{
			_ret set[count _ret,["V_PlateCarrier1_blk",nil,1000]];	
		};
	};
	
	//Backpacks
	case 4:
	{
		_ret =
		[
			["B_Kitbag_cbr",nil,1000],
			["B_FieldPack_cbr",nil,1000],
			["B_AssaultPack_cbr",nil,1000],
			["B_Bergen_sgg",nil,1000],
			["B_Carryall_cbr",nil,1000]
		];
	};
};

_ret;