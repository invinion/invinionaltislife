﻿#include <macro.h>
/*
	File: fn_weaponShopCfg.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration file for the weapon shops.
	
	Return:
	String: Close the menu
	Array: 
	[Shop Name,
	[ //Array of items to add to the store
		[classname,Custom Name (set nil for default),price]
	]]
*/
private["_shop"];
_shop = [_this,0,"",[""]] call BIS_fnc_param;
if(_shop == "") exitWith {closeDialog 0}; //Bad shop type passed.

switch(_shop) do
{
	case "cop_weapons":
	{
		switch(true) do
		{
			case (playerSide != west): {"Du bist kein Polizist! Geh zum Arbeitsamt und such dir eine neue Arbeit!!!"};
			default
			{
				switch ((__GETC__(life_coplevel))) do {
					case 2: {["Wachtmeister Shop",
								[
									["hgun_P07_snds_F","Tazer",500],
									["30Rnd_9x21_Mag",nil,75],
									["SMG_02_F",nil,7500],
									["30Rnd_9x21_Mag",nil,75],
									["optic_Aco","ACO",300],
									["muzzle_snds_L","9mm Schalldämpfer",7500]
									]
								];}; 
					case 3: {["Oberwachtmeister Shop",
								[
									["hgun_P07_snds_F","Tazer",500],
									["30Rnd_9x21_Mag",nil,75],
									["SMG_02_F",nil,5000],
									["30Rnd_9x21_Mag",nil,75],
									["arifle_SDAR_F",nil,7500],
									["30Rnd_556x45_Stanag",nil,100],
									["optic_Aco","ACO",300],
									["muzzle_snds_L","9mm Schalldämpfer",7500]
								]
							];};
					case 4: {["Kommissar Shop",
								[
									["hgun_P07_snds_F","Tazer",500],
									["30Rnd_9x21_Mag",nil,75],
									["SMG_02_F",nil,5000],
									["30Rnd_9x21_Mag",nil,75],
									["arifle_SDAR_F",nil,7500],
									["30Rnd_556x45_Stanag",nil,100],
									["arifle_MXC_Black_F",nil,20000],
									["30Rnd_65x39_caseless_mag",nil,150],
									["30Rnd_65x39_caseless_mag_Tracer","Gummigeschoss Munition",150],
									["optic_Aco","ACO",300],
									["optic_Hamr","RCO",300],
									["optic_Arco","ARCO",300],
									["optic_MRCO","MRCO",300],
									["acc_pointer_IR","Laser",5000],
									["muzzle_snds_L","9mm Schalldämpfer",7500],
									["muzzle_snds_H","6.5mm Schalldämpfer",7500]
								]
							];};
					case 5: {["Hauptkommissar Shop",
								[
									["hgun_P07_snds_F","Tazer",500],
									["30Rnd_9x21_Mag",nil,75],
									["SMG_02_F",nil,5000],
									["30Rnd_9x21_Mag",nil,75],
									["arifle_SDAR_F",nil,7500],
									["30Rnd_556x45_Stanag",nil,100],
									["arifle_MXC_Black_F",nil,10000],
									["arifle_MX_Black_F",nil,20000],
									["30Rnd_65x39_caseless_mag",nil,150],
									["30Rnd_65x39_caseless_mag_Tracer","Gummigeschoss Munition",150],
									["arifle_Katiba_F",nil,20000],
									["30Rnd_65x39_caseless_green",nil,150],
									["optic_Aco","ACO",300],
									["optic_Hamr","RCO",300],
									["optic_Arco","ARCO",300],
									["optic_MRCO","MRCO",300],
									["optic_DMS","DMS",150],
									["acc_pointer_IR","Laser",5000],
									["muzzle_snds_L","9mm Schalldämpfer",7500]
									]
								];};
					case 6: {["Unteroffizier Shop",
								[
									["hgun_P07_snds_F","Tazer",500],
									["30Rnd_9x21_Mag",nil,75],
									["SMG_02_F",nil,5000],
									["30Rnd_9x21_Mag",nil,75],
									["arifle_SDAR_F",nil,7500],
									["30Rnd_556x45_Stanag",nil,100],
									["arifle_MXC_Black_F",nil,10000],
									["arifle_MX_Black_F",nil,20000],
									["arifle_MX_GL_Black_F","Noobtube",50000],
									["arifle_MXM_Black_F",nil,30000],
									["30Rnd_65x39_caseless_mag",nil,150],
									["30Rnd_65x39_caseless_mag_Tracer","Gummigeschoss Munition",150],
									["arifle_Katiba_F",nil,20000],
									["30Rnd_65x39_caseless_green",nil,150],
									["optic_Aco","ACO",300],
									["optic_Hamr","RCO",300],
									["optic_Arco","ARCO",300],
									["optic_MRCO","MRCO",300],
									["optic_DMS","DMS",150],
									["optic_SOS","SOS",300],
									["acc_pointer_IR","Laser",5000],
									["1Rnd_HE_Grenade_shell",nil,10000],
									["muzzle_snds_L","9mm Schalldämpfer",7500],
									["muzzle_snds_H","6.5mm Schalldämpfer",7500]
								]
							];};
					case 7: {["Offizier Shop",
								[
									["hgun_P07_snds_F","Tazer",500],
									["30Rnd_9x21_Mag",nil,75],
									["SMG_02_F",nil,5000],
									["30Rnd_9x21_Mag",nil,75],
									["arifle_SDAR_F",nil,7500],
									["30Rnd_556x45_Stanag",nil,100],
									["arifle_MXC_Black_F",nil,10000],
									["arifle_MX_Black_F",nil,20000],
									["arifle_MX_GL_Black_F","Noobtube",50000],
									["arifle_MXM_Black_F",nil,30000],
									["30Rnd_65x39_caseless_mag",nil,150],
									["30Rnd_65x39_caseless_mag_Tracer","Gummigeschoss Munition",150],
									["arifle_Katiba_F",nil,20000],
									["30Rnd_65x39_caseless_green",nil,150],
									["srifle_EBR_F",nil,50000],
									["20Rnd_762x51_Mag",nil,150],
									["optic_Aco","ACO",300],
									["optic_Hamr","RCO",300],
									["optic_Arco","ARCO",300],
									["optic_MRCO","MRCO",300],
									["optic_DMS","DMS",150],
									["optic_SOS","SOS",300],
									["acc_pointer_IR","Laser",5000],
									["1Rnd_HE_Grenade_shell",nil,10000],
									["SmokeShellGreen",nil,7500],
									["muzzle_snds_L","9mm Schalldämpfer",7500],
									["muzzle_snds_H","6.5mm Schalldämpfer",7500],
									["muzzle_snds_B","7.62mm Schalldämpfer",7500]
								]
							];};
					case 8: {["Polizeirat Shop",
								[
									["hgun_P07_snds_F","Tazer",500],
									["30Rnd_9x21_Mag",nil,75],
									["SMG_02_F",nil,5000],
									["30Rnd_9x21_Mag",nil,75],
									["arifle_SDAR_F",nil,7500],
									["30Rnd_556x45_Stanag",nil,100],
									["arifle_MXC_Black_F",nil,10000],
									["arifle_MX_Black_F",nil,20000],
									["arifle_MX_GL_Black_F","Noobtube",50000],
									["arifle_MXM_Black_F",nil,30000],
									["30Rnd_65x39_caseless_mag",nil,150],
									["30Rnd_65x39_caseless_mag_Tracer","Gummigeschoss Munition",150],
									["arifle_Katiba_F",nil,20000],
									["30Rnd_65x39_caseless_green",nil,150],
									["srifle_EBR_F",nil,50000],
									["20Rnd_762x51_Mag",nil,150],
									["LMG_Mk200_F",nil,60000],
									["200Rnd_65x39_cased_Box",nil,150],
									["optic_Aco","ACO",300],
									["optic_Hamr","RCO",300],
									["optic_Arco","ARCO",300],
									["optic_MRCO","MRCO",300],
									["optic_DMS","DMS",150],
									["optic_SOS","SOS",300],
									["acc_pointer_IR","Laser",5000],
									["1Rnd_HE_Grenade_shell",nil,10000],
									["SmokeShellGreen",nil,7500],
									["muzzle_snds_L","9mm Schalldämpfer",7500],
									["muzzle_snds_H","6.5mm Schalldämpfer",7500],
									["muzzle_snds_B","7.62mm Schalldämpfer",7500],
									["muzzle_snds_H_MG","MK100 Schalldämpfer",7500]
									]
								];};				
					case 9: {["1 General Shop",
								[
									["hgun_P07_snds_F","Tazer",500],
									["30Rnd_9x21_Mag",nil,75],
									["SMG_02_F",nil,5000],
									["30Rnd_9x21_Mag",nil,75],
									["arifle_SDAR_F",nil,7500],
									["30Rnd_556x45_Stanag",nil,100],
									["arifle_MXC_Black_F",nil,10000],
									["arifle_MX_Black_F",nil,20000],
									["arifle_MX_GL_Black_F","Noobtube",50000],
									["arifle_MXM_Black_F",nil,30000],
									["30Rnd_65x39_caseless_mag",nil,150],
									["30Rnd_65x39_caseless_mag_Tracer","Gummigeschoss Munition",150],
									["arifle_Katiba_F",nil,20000],
									["30Rnd_65x39_caseless_green",nil,150],
									["srifle_EBR_F",nil,50000],
									["20Rnd_762x51_Mag",nil,150],
									["LMG_Mk200_F",nil,60000],
									["200Rnd_65x39_cased_Box",nil,150],
									["srifle_LRR_camo_F",nil,150000],
								    ["7Rnd_408_Mag",nil,150],
									["optic_Aco","ACO",300],
									["optic_Hamr","RCO",300],
									["optic_Arco","ARCO",300],
									["optic_MRCO","MRCO",300],
									["optic_DMS","DMS",150],
									["optic_SOS","SOS",300],
									["optic_NVS","NVS",300],
									["acc_pointer_IR","Laser",5000],
									["1Rnd_HE_Grenade_shell",nil,10000],
									["SmokeShellGreen",nil,7500],
									["muzzle_snds_L","9mm Schalldämpfer",7500],
									["muzzle_snds_H","6.5mm Schalldämpfer",7500],
									["muzzle_snds_B","7.62mm Schalldämpfer",7500],
									["muzzle_snds_H_MG","MK100 Schalldämpfer",7500]
								]
							];};							
					case 10: {["General Shop",
								[
									["hgun_P07_snds_F","Tazer",500],
									["30Rnd_9x21_Mag",nil,75],
									["SMG_02_F",nil,5000],
									["30Rnd_9x21_Mag",nil,75],
									["arifle_SDAR_F",nil,7500],
									["30Rnd_556x45_Stanag",nil,100],
									["arifle_MXC_Black_F",nil,10000],
									["arifle_MX_Black_F",nil,20000],
									["arifle_MX_GL_Black_F","Noobtube",50000],
									["arifle_MXM_Black_F",nil,30000],
									["30Rnd_65x39_caseless_mag",nil,150],
									["30Rnd_65x39_caseless_mag_Tracer","Gummigeschoss Munition",150],
									["arifle_Katiba_F",nil,20000],
									["30Rnd_65x39_caseless_green",nil,150],
									["srifle_EBR_F",nil,50000],
									["20Rnd_762x51_Mag",nil,150],
									["LMG_Mk200_F",nil,60000],
									["200Rnd_65x39_cased_Box",nil,150],
									["srifle_LRR_camo_F",nil,150000],
								    ["7Rnd_408_Mag",nil,150],
									["srifle_GM6_camo_F",nil,150000],
								    ["5Rnd_127x108_Mag",nil,150],
									["optic_Aco","ACO",300],
									["optic_Hamr","RCO",300],
									["optic_Arco","ARCO",300],
									["optic_MRCO","MRCO",300],
									["optic_DMS","DMS",150],
									["optic_SOS","SOS",300],
									["optic_NVS","NVS",300],
									["acc_pointer_IR","Laser",5000],
									["1Rnd_HE_Grenade_shell",nil,10000],
									["SmokeShellGreen",nil,7500],
									["muzzle_snds_L","9mm Schalldämpfer",7500],
									["muzzle_snds_H","6.5mm Schalldämpfer",7500],
									["muzzle_snds_B","7.62mm Schalldämpfer",7500],
									["muzzle_snds_H_MG","MK100 Schalldämpfer",7500]
								]
								];
							};									
				};
			};
		};
	};
	
	case "rebel":
	{
		switch(true) do
		{
			case (playerSide != civilian): {"You are not a civilian!"};
			case (!license_civ_rebel): {"You don't have a Rebel training license!"};
			default
			{
				if(__GETC__(life_rebel) == 1) then {
					["Whitelisted Rebellen Shop",
						[
							["hgun_Pistol_heavy_01_F",nil,7500],
							["11Rnd_45ACP_Mag",nil,200],
							["SMG_01_F",nil,10000],
							["30Rnd_45ACP_Mag_SMG_01",nil,150],
							["arifle_SDAR_F",nil,20000],
							["20Rnd_556x45_UW_mag",nil,125],
							["arifle_TRG20_F",nil,25000],
							["arifle_TRG21_GL_F",nil,25000],
							["30Rnd_556x45_Stanag",nil,300],
							["arifle_Mk20_F",nil,50000],
							["arifle_Mk20_GL_F",nil,50000],
							["30Rnd_556x45_Stanag",nil,300],
							["arifle_MXC_F",nil,60000],
							["30Rnd_65x39_caseless_mag",nil,300],
							["arifle_Katiba_F",nil,100000],
							["arifle_Katiba_GL_F",nil,100000],
							["30Rnd_65x39_caseless_green",nil,275],
							["arifle_MX_F",nil,120000],
							["arifle_MX_GL_F",nil,120000],
							["30Rnd_65x39_caseless_mag",nil,300],
							["arifle_MXM_F",nil,300000],
							["30Rnd_65x39_caseless_mag",nil,300],
							["srifle_EBR_F",nil,300000],
							["20Rnd_762x51_Mag",nil,1000],
							["arifle_MX_SW_F",nil,500000],
							["100Rnd_65x39_caseless_mag",nil,3500],
							["LMG_Zafir_F",nil,1000000],
							["150Rnd_762x51_Box",nil,10000],
							["srifle_GM6_F",nil,1000000],
							["5Rnd_127x108_Mag",nil,7500],
							["srifle_LRR_F",nil,1000000],
							["7Rnd_408_Mag",nil,7500],
							["optic_ACO_grn",nil,3500],
							["optic_Holosight",nil,5000],
							["optic_Hamr",nil,10000],
							["optic_Arco",nil,10000],
							["optic_MRCO",nil,10000],
							["optic_DMS",nil,15000],
							["optic_SOS",nil,20000],
							["optic_LRPS",nil,25000],
							["optic_NVS",nil,30000],
							["UGL_FlareWhite_F",nil,5000],
							["UGL_FlareRed_F",nil,5000],
							["UGL_FlareYellow_F",nil,5000],
							["UGL_FlareCIR_F",nil,5000],
							["UGL_FlareGreen_F",nil,5000],
							["1Rnd_HE_Grenade_shell",nil,50000],
							["ClaymoreDirectionalMine_Remote_Mag",nil,1500000],		
							["acc_flashlight",nil,5000],
							["acc_pointer_IR",nil,15000],
							["ItemGPS",nil,250],
							["Binocular",nil,500],
							["NVGoggles",nil,2500],
							["FirstAidKit",nil,250],
							["ToolKit",nil,5000]
						]
					];
				}else{
					["Rebellen Shop",
						[
							["hgun_Pistol_heavy_01_F",nil,7500],
							["11Rnd_45ACP_Mag",nil,200],
							["SMG_01_F",nil,10000],
							["30Rnd_45ACP_Mag_SMG_01",nil,150],
							["arifle_SDAR_F",nil,20000],
							["20Rnd_556x45_UW_mag",nil,125],
							["arifle_TRG20_F",nil,25000],
							["30Rnd_556x45_Stanag",nil,300],
							["arifle_Mk20_F",nil,50000],
							["30Rnd_556x45_Stanag",nil,300],
							["arifle_MXC_F",nil,60000],
							["30Rnd_65x39_caseless_mag",nil,300],
							["arifle_Katiba_F",nil,100000],
							["30Rnd_65x39_caseless_green",nil,275],
							["arifle_MX_F",nil,120000],
							["30Rnd_65x39_caseless_mag",nil,300],
							["optic_ACO_grn",nil,3500],
							["optic_Holosight",nil,5000],
							["optic_Hamr",nil,10000],
							["optic_Arco",nil,10000],
							["optic_MRCO",nil,10000],
							["acc_flashlight",nil,5000],
							["acc_pointer_IR",nil,15000],
							["ItemGPS",nil,250],
							["Binocular",nil,500],
							["NVGoggles",nil,2500],
							["FirstAidKit",nil,250],
							["ToolKit",nil,5000]
						]
					];
				};
			};
		};
	};
	
	case "gun":
	{
		switch(true) do
		{
			case (playerSide != civilian): {"You are not a civilian!"};
			case (!license_civ_gun): {"You don't have a Firearms license!"};
			default
			{
				["Waffenhändler",
					[
						["hgun_Rook40_F",nil,6500],
						["hgun_Pistol_heavy_02_F",nil,9850],
						["hgun_ACPC2_F",nil,11500],
						["hgun_PDW2000_F",nil,20000],
						["optic_ACO_grn_smg",nil,2500],
						["V_Rangemaster_belt",nil,4900],
						["16Rnd_9x21_Mag",nil,25],
						["9Rnd_45ACP_Mag",nil,45],
						["6Rnd_45ACP_Cylinder",nil,50],
						["30Rnd_9x21_Mag",nil,75],
						["hgun_Pistol_Signal_F","Signalpistole",3000],
                        ["6Rnd_GreenSignal_F",nil,3000],
                        ["6Rnd_RedSignal_F",nil,3000]
					]
				];
			};
		};
	};
	
	case "gang":
	{
		switch(true) do
		{
			case (playerSide != civilian): {"You are not a civilian!"};
			default
			{
				["Hideout Armament",
					[
						["hgun_Rook40_F",nil,1500],
						["hgun_Pistol_heavy_02_F",nil,2500],
						["hgun_ACPC2_F",nil,4500],
						["hgun_PDW2000_F",nil,9500],
						["optic_ACO_grn_smg",nil,950],
						["V_Rangemaster_belt",nil,1900],
						["16Rnd_9x21_Mag",nil,25],
						["9Rnd_45ACP_Mag",nil,45],
						["6Rnd_45ACP_Cylinder",nil,50],
						["30Rnd_9x21_Mag",nil,75]
					]
				];
			};
		};
	};
	
	case "genstore":
	{
		["Waffenhändler",
			[
				["Binocular",nil,150],
				["ItemGPS",nil,100],
				["ToolKit",nil,5000],
				["FirstAidKit",nil,150],
				["NVGoggles",nil,2000],
				["ItemRadio","Handy",200]
			]
		];
	};
	
	case "adac":
	{
		["Altis General Store",
			[
				["Binocular",nil,150],
				["ItemGPS",nil,100],
				["ToolKit",nil,100],
				["FirstAidKit",nil,150],
				["NVGoggles",nil,2000],
				["Chemlight_yellow",nil,300]
			]
		];
	};
	
	case "donator":
	{
		if((__GETC__(life_donator))>3) then
		{
			["Donator Level 1 Shop",
				[
					["hgun_Pistol_heavy_01_F",nil,7500],
					["11Rnd_45ACP_Mag",nil,200],
					["SMG_01_F",nil,10000],
					["30Rnd_45ACP_Mag_SMG_01",nil,150],
					["arifle_SDAR_F",nil,20000],
					["20Rnd_556x45_UW_mag",nil,125],
					["arifle_TRG20_F",nil,25000],
					["30Rnd_556x45_Stanag",nil,300],
					["arifle_Mk20_F",nil,50000],
					["30Rnd_556x45_Stanag",nil,300],
					["arifle_MXC_F",nil,60000],
					["30Rnd_65x39_caseless_mag",nil,300],
					["arifle_Katiba_F",nil,100000],
					["30Rnd_65x39_caseless_green",nil,275],
					["arifle_MX_F",nil,120000],
					["30Rnd_65x39_caseless_mag",nil,300],
					["arifle_MXM_F",nil,300000],
					["30Rnd_65x39_caseless_mag",nil,300],
					["srifle_EBR_F",nil,300000],
					["20Rnd_762x51_Mag",nil,1000],
					["arifle_MX_SW_F",nil,500000],
					["100Rnd_65x39_caseless_mag",nil,3500],
					["LMG_Zafir_F",nil,1000000],
					["150Rnd_762x51_Box",nil,10000],
					["srifle_GM6_F",nil,1000000],
					["5Rnd_127x108_Mag",nil,7500],
					["optic_ACO_grn",nil,3500],
					["optic_Holosight",nil,5000],
					["optic_Hamr",nil,10000],
					["optic_Arco",nil,10000],
					["optic_MRCO",nil,10000],
					["optic_DMS",nil,15000],
					["optic_SOS",nil,20000],
					["optic_LRPS",nil,25000],
					["optic_NVS",nil,30000],
					["acc_flashlight",nil,5000],
					["acc_pointer_IR",nil,15000],
					["ItemGPS",nil,250],
					["Binocular",nil,500],
					["NVGoggles",nil,2500],
					["FirstAidKit",nil,250],
					["ToolKit",nil,5000]
				]
			];
		};
	};
};
