#include <macro.h>
/*
	File: fn_initCop.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Cop Initialization file.
*/
private["_end"];
player addRating 9999999;
waitUntil {!(isNull (findDisplay 46))};
_end = false;
if(life_blacklisted) exitWith
{
	["Blacklisted",false,true] call BIS_fnc_endMission;
	sleep 30;
};

if(!(str(player) in ["nope"])) then {
=======
[] spawn
{
	while {true} do
    {
		
		switch ((uniform player)) do {
					case "U_Rangemaster": {  player setObjectTextureGlobal [0,"textures\clothing\polizei_anwaerter.paa"];
											if(backpack player != "") then { (unitBackpack player) setObjectTextureGlobal [0,""];};
											sleep 5;}; 
					case "U_B_CombatUniform_mcam": {
											if(__GETC__(life_sek) == 1)then{
												player setObjectTextureGlobal [0,"textures\clothing\sek_uniform.paa"];
											}else{
												if(__GETC__(life_coplevel) > 1) then
												{
													player setObjectTextureGlobal [0,"textures\clothing\polizei_meister.paa"];
												};
												if(__GETC__(life_coplevel) > 3) then
												{
													player setObjectTextureGlobal [0,"textures\clothing\police_kommisar.paa"];
												};
												if(__GETC__(life_coplevel) > 5) then
												{
													player setObjectTextureGlobal [0,"textures\clothing\polizei_offizier.paa"];
												};
												if(__GETC__(life_coplevel) > 6) then
												{
													player setObjectTextureGlobal [0,"textures\clothing\polizei_polizeirat.paa"];
												};
												if(__GETC__(life_coplevel) > 7) then
												{
													player setObjectTextureGlobal [0,"textures\clothing\polizei_general.paa"];
												};
											};
											if(backpack player != "") then { (unitBackpack player) setObjectTextureGlobal [0,""];};
											sleep 5;
						};
											
											
					case "U_B_CombatUniform_mcam_tshirt": {player setObjectTextureGlobal [0,"textures\clothing\sek_uniform.paa"];
											if(backpack player != "") then { (unitBackpack player) setObjectTextureGlobal [0,""];};
											sleep 5;};
					case "U_B_survival_uniform": {player setObjectTextureGlobal [0,"textures\clothing\sek_uniform.paa"];
											if(backpack player != "") then { (unitBackpack player) setObjectTextureGlobal [0,""];};
											sleep 5;};
					default { sleep 30;};
		};
		

    };
};

if(!(str(player) in ["nope"])) then {
	if((__GETC__(life_coplevel) == 0) && (__GETC__(life_adminlevel) == 0)) then {
		["NotWhitelisted",false,true] call BIS_fnc_endMission;
		sleep 35;
	};
};

//player setVariable["rank",(__GETC__(life_coplevel)),true];
//player setVariable["sek",(__GETC__(life_sek)),true];

[] call life_fnc_spawnMenu;
waitUntil{!isNull (findDisplay 38500)}; //Wait for the spawn selection to be open.
waitUntil{isNull (findDisplay 38500)}; //Wait for the spawn selection to be done.



